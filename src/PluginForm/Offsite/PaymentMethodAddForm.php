<?php

namespace Drupal\commerce_slydepay\PluginForm\Offsite;

use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use SoapClient;
use SoapHeader;

/**
 * Payment form for Offsite Gateway method.
 */
class PaymentMethodAddForm extends PaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_paypal\Plugin\Commerce\PaymentGateway\ExpressCheckoutInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $configuration = $payment_gateway_plugin->getConfiguration();
    // API credential. Set headers.
    $header_parameters = [
      'APIVersion' => $configuration['api_version'],
      'MerchantKey' => $configuration['merchant_key'],
      'MerchantEmail' => $configuration['merchant_email'],
      'SvcType' => $configuration['service_type'],
      'UseIntMode' => $configuration['integration_mode'],
    ];

    // The Order object
    $order = $payment->getOrder();
    $order_items = $order->getItems();
    $products = [];
    // Get the line items from the order.
    foreach ($order_items as $order_item) {
      $product_variation = $order_item->getPurchasedEntity();
      $product_sku = $product_variation->getSku();
      $unit_price = $order_item->getUnitPrice()->getNumber();
      $sub_total = $order_item->getTotalPrice()->getNumber();

      // OrderItems to send to the API
      $products[] = [
        'ItemCode' => $product_sku,
        'ItemName' => $order_item->getTitle(),
        'UnitPrice' => number_format($unit_price, 2),
        'Quantity' => $order_item->getQuantity(),
        'SubTotal' => number_format($sub_total, 2),
      ];
    }

    $order_subtotal = number_format($order->getSubtotalPrice()->getNumber(), 2);
    $order_total = number_format($order->getTotalPrice()->getNumber(), 2);
    $order_id = $payment->getOrderId();
    $url = $configuration['redirect_url'];

    $data = [
      'orderId' => $order_id,
      'total' => $order_total,
      'shippingCost' => 0,
      'comment1' => 'product 1',
      'taxAmount' => 0,
      'subTotal' => $order_subtotal,
      'orderItems' => [
        'OrderItem' => $products,
      ],
    ];
    $api_server = $configuration['api_server'];
    $namespace = 'http://www.i-walletlive.com/payLIVE';
    $soapClient = new SoapClient($api_server);
    $soap = new SoapHeader($namespace, 'PaymentHeader', $header_parameters);
    $soapClient->__setSoapHeaders($soap);
    $slydepay_responses = [];
    $result = $soapClient->ProcessPaymentOrder($data);
    if (isset($result->ProcessPaymentOrderResult) && strlen($result->ProcessPaymentOrderResult) == 36) {
      $slydepay_responses['pay_token'] = $result->ProcessPaymentOrderResult;
    }
    elseif (isset($result->ProcessPaymentOrderResult)) {
      $slydepay_responses['error'] = $result->ProcessPaymentOrderResult;
    }
    else {
      $slydepay_responses['error'] = 'Unknow ERROR! Order proccessing failed!';
    }

    if ($slydepay_responses['pay_token'] != '') {
      $data+= [
        'pay_token' => $slydepay_responses['pay_token'],
      ];
      // Build redirect url.
      $options = [
        'absolute' => TRUE,
        'query'    => ['pay_token' => $slydepay_responses['pay_token']],
      ];
      $redirect_url = Url::fromUri($url, $options)->toString();
      return $this->buildRedirectForm($form, $form_state, $redirect_url, $data, 'post');
    }
    else {
      $error_message = 'Order Id already exists. Please try placing another order.';
      $error = $slydepay_responses['error'];
      if ($error == 'Error: 1') {
        $error_message = 'Merchant Credentials wrong check email or merchant key';
      }
      elseif ($error == 'Error: -1') {
        $error_message = 'Technical error contact <a href="https://support.i-walletlive.com/">Slydepay Support</a>';
      }
      elseif ($error == 'Error: 2') {
        $error_message = 'Merchant not confirmed';
      }
      elseif ($error == 'Error: 3') {
        $error_message = 'Merchant account not verified';
      }
      elseif ($error == 'Error: 4') {
        $error_message = 'Integration Mode set to OFF in account. Set UseIntMode param to "0" or switch integration mode on in account.';
      }
      \Drupal::messenger()->addError(t($error_message));
    }
  }

}