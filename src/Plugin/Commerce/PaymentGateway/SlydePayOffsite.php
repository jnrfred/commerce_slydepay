<?php

namespace Drupal\commerce_slydepay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_slydepay_offsite",
 *   label = @Translation("Slydepay (Offsite-payement)"),
 *   display_label = @Translation("Slydepay"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_slydepay\PluginForm\Offsite\PaymentMethodAddForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "debit", "mastercard", "visa",
 *   },
 * )
 */
class SlydePayOffsite extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'merchant_key' => '',
        'merchant_email' => '',
        'service_type' => 'C2B',
        'callback_url' => '',
        'redirect_url' => '',
        'api_version' => 1.4,
        'api_server' => 'https://app.slydepay.com.gh/webservices/paymentservice.asmx?wsdl',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['merchant_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Key'),
      '#required' => TRUE,
      '#description' => $this->t('API Key is generated from the settings page in your Slydepay business account.'),
      '#default_value' => $this->configuration['merchant_key'],
    ];

    $form['merchant_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Email'),
      '#required' => TRUE,
      '#description' => $this->t('The email address for your Slydepay business account.'),
      '#default_value' => $this->configuration['merchant_email'],
    ];

    $form['api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Version'),
      '#required' => TRUE,
      '#description' => $this->t('Specify the API version from which you are integrating your application. For this API version specify (1.4).'),
      '#default_value' => $this->configuration['api_version'],
    ];

    $form['callback_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Callback URL'),
      '#required' => TRUE,
      '#description' => $this->t('The is the url Slydepay will post back to once transaction is complete.'),
      '#default_value' => $this->configuration['callback_url'],
    ];

    $form['redirect_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect URL'),
      '#required' => TRUE,
      '#description' => $this->t('Usually this is https://app.slydepay.com.gh/paylive/detailsnew.aspx.'),
      '#default_value' => $this->configuration['redirect_url'],
    ];


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_key'] = $values['merchant_key'];
      $this->configuration['merchant_email'] = $values['merchant_email'];
      $this->configuration['api_version'] = $values['api_version'];
      $this->configuration['callback_url'] = $values['callback_url'];
      $this->configuration['redirect_url'] = $values['redirect_url'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $status = $request->request->get('status');
    $customer_ref = $request->request->get('cust_ref');
    $transaction_id = $request->request->get('transac_id');
    if ($status == 0) {
      $payment_storage =  \Drupal::entityTypeManager()->getStorage('commerce_payment');
      $payment = $payment_storage->create([
        'state' => 'completed',
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->entityId,
        'order_id' => $order->id(),
        'test' => $this->getMode() == 'test',
        'remote_id' => $transaction_id,
        'authorized' => \Drupal::time()->getRequestTime(),
      ]);

      $payment->save();
      \Drupal::messenger()->addMessage(t('Your payment was successful with Order ID : @orderid and Transaction ID : @transaction_id', [
        '@orderid' => $order->id(),
        '@transaction_id' => $transaction_id,
      ]));
    }
    elseif ($status == -1) {
      \Drupal::messenger()->addError(t('Technical error contact <a href="https://support.i-walletlive.com/">Slydepay Support</a>'));
    }
    elseif ($status == -2) {
      \Drupal::messenger()->addError(t('User cancelled transaction'));
    }
    else {
      throw new PaymentGatewayException();
    }

  }

}